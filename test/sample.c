#include <stdio.h>
#include <sys/syscall.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include "../android-tegra-3.1/include/linux/orientation.h"

static struct orientation_range *create_sample_range(
		int a, int p, int r, int ar, int pr, int rr)
{
	struct orientation_range *range;
	range = (struct orientation_range *)malloc(
			sizeof(struct orientation_range));
	range->orient.azimuth = a;
	range->orient.pitch = p;
	range->orient.roll = r;
	range->azimuth_range = ar;
	range->pitch_range = pr;
	range->roll_range = rr;
	return range;
}

static void call(int syscallnum, int a, int ar, int p,
		int pr, int r, int rr, int sleep_time)
{
	struct orientation_range *or = create_sample_range(
			a, p , r, ar, pr, rr);
	printf(">or:%d+-%d, %d+-%d, %d+-%d <", a, ar, p, pr, r, rr);
	fflush(stdout);
	printf("> s: sys[%d]: p[%d] == %ld", syscallnum,
			getpid(), syscall(syscallnum, or));
	printf(" <> process resumed. sleep %d sec\n", sleep_time);
	fflush(stdout);
	sleep(sleep_time);
}

int main(void)
{
	printf("calling sample\n");
	struct dev_orientation *t_orient;
	t_orient = (struct dev_orientation *)malloc(
			sizeof(struct dev_orientation));

	while (1) {
		call(378, 0, 180, 0, 10, 0, 10, 0);
		call(380, 0, 180, 0, 10, 0, 10, 0);
	}

	printf(" >sample: exiting sample\n");
	return 0;
}
