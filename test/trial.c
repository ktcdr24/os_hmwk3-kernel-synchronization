#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <math.h>
#include <gmp.h>
#include <sys/syscall.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include "../android-tegra-3.1/include/linux/orientation.h"
#include "prime.h"

static mpz_t one;
static mpz_t two;

static void find_factors(mpz_t base)
{
	char *str;
	int res;
	mpz_t i;
	mpz_t half;

	mpz_init_set_str(i, "2", 10);
	mpz_init(half);
	mpz_cdiv_q(half, base, two);

	str = mpz_to_str(base);
	if (!str)
		return;

	/*
	 * We simply return the prime number itself if the base is prime.
	 * (We use the GMP probabilistic function with 10 repetitions).
	 */
	res = mpz_probab_prime_p(base, 10);
	if (res) {
		printf("TRIAL : %s is a prime number\n", str);
		free(str);
		return;
	}

	printf("TRIAL : Prime factors for %s are:  ", str);
	free(str);
	do {
		if (mpz_divisible_p(base, i) && verify_is_prime(i)) {
			str = mpz_to_str(i);
			if (!str)
				return;
			printf("%s ,", str);
			free(str);
		}
		mpz_nextprime(i, i);
	} while (mpz_cmp(i, half) <= 0);
	printf("\n");
}

int main(int argc, const char *argv[])
{
	mpz_t largenum;
	FILE *fp;
	size_t ret;
	struct orientation_range *face_d;
	face_d = (struct orientation_range *)malloc(
			sizeof(struct orientation_range));

	face_d->orient.azimuth = 0;
	face_d->azimuth_range = 180;
	face_d->orient.pitch = 180;
	face_d->pitch_range = 10;
	face_d->orient.roll = 0;
	face_d->roll_range = 10;

	mpz_init_set_str(one, "1", 10);
	mpz_init_set_str(two, "2", 10);

	mpz_init(largenum);

	while (1) {
		syscall(377, face_d);
		fp = fopen("/data/misc/integer", "r");
		ret = mpz_inp_str(largenum, fp, 10);
		if (ret == 0) {
			fclose(fp);
			syscall(379, face_d);
			continue;
		}
		find_factors(largenum);
		fclose(fp);
		syscall(379, face_d);
	}

	return EXIT_SUCCESS;
}
