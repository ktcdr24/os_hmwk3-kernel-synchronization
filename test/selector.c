#include <stdio.h>
#include <stdlib.h>
#include <sys/syscall.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <string.h>
#include <gmp.h>
#include "prime.h"
#include "../android-tegra-3.1/include/linux/orientation.h"

int main(int argc, char *argv[])
{
	mpz_t i;
	mpz_t one;
	char *i_str;
	FILE *fp;
	struct orientation_range *face_d;
	face_d = (struct orientation_range *)malloc(
			sizeof(struct orientation_range));

	face_d->orient.azimuth = 0;
	face_d->azimuth_range = 180;
	face_d->orient.pitch = 180;
	face_d->pitch_range = 10;
	face_d->orient.roll = 0;
	face_d->roll_range = 10;

	mpz_init_set_str(one, "1", 10);

	if (argc != 2)
		printf("usage: %s integer\n", argv[0]);
	else {
		mpz_init_set_str(i, argv[1], 10);
		while (1) {
			/* obtain write lock */
			syscall(378, face_d);
			fp = fopen("/data/misc/integer", "w+");
			i_str = mpz_to_str(i);
			fwrite(i_str, strlen(i_str), 1, fp);
			fclose(fp);
			printf("Value: %s\n", i_str);
			/* release write lock */
			syscall(380, face_d);
			mpz_add(i, i, one);
		}
	}
	return 0;
}
