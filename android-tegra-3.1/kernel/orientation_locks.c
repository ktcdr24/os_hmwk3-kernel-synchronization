#include <linux/uaccess.h>
#include <linux/slab.h>
#include <linux/syscalls.h>
#include <linux/orientation_locks.h>
#include <linux/orientation.h>
#include <linux/sched.h>

#define READ_LOCK_TYPE 'r'
#define WRITE_LOCK_TYPE 'w'

static struct area_desc desc_head;
static int desc_head_size = -1;
static DECLARE_RWSEM(mr_rwsem);
static DECLARE_RWSEM(mr_rwsem1);

static int adjust_azimuth(int deg)
{
	if (deg >= 0 && deg <= 360)
		return deg;
	if (deg < 0) {
		while (deg < 0)
			deg += 360;
	}
	return deg % 360;
}

static int adjust_pitch(int deg)
{
	if (deg >= -180 && deg <= 180)
		return deg;
	if (deg > 180)
		return deg - 360;
	if (deg < -180)
		return deg + 360;
	return deg;
}

static int adjust_roll(int deg)
{
	if (deg >= -90 && deg <= 90)
		return deg;
	if (deg > 90)
		return 90;
	if (deg < -90)
		return -90;
	return deg;
}

static int p_to_360(int p)
{
	if (p < 0)
		p += 360;
	return p;
}

/*
static void print_area_description(struct area_desc *temp)
{
	struct list_head *pos;
	struct ps_node *ps_node_curr;
	printk("\t\t<< [a(%d,%d):p(%d,%d):r(%d,%d)]\n",
			temp->azimuth_low, temp->azimuth_high,
			temp->pitch_low, temp->pitch_high,
			temp->roll_low, temp->roll_high);
	printk("\t\t[ps_wait:\n");
	list_for_each(pos, &temp->ps_waiting.list) {
		ps_node_curr = list_entry(pos, struct ps_node, list);
		printk(PR_INFO, "\t\t\t{pid=%d,lock=%c},\n",
				task_pid_nr(ps_node_curr->task),
				ps_node_curr->lock_type);
	}
	printk("\t\t]\n");
	printk("\t\t[ps_grant:\n");
	list_for_each(pos, &temp->ps_granted.list) {
		ps_node_curr = list_entry(pos, struct ps_node, list);
		printk("\t\t\t{pid=%d,lock=%c},\n", task_pid_nr(
			ps_node_curr->task), ps_node_curr->lock_type);
	}
	printk("\t\t] >>\n");
}
*/

/*
static void print_area_desc(void)
{
	struct list_head *pos;
	struct area_desc *tmp;
	printk(" $$ print_area_desc\n");
	list_for_each(pos, &desc_head.list) {
		tmp = list_entry(pos, struct area_desc, list);
		print_area_description(tmp);
	}
	printk(" $$\n");
}
*/

static int area_desc_equals(struct area_desc *temp1, struct area_desc *temp2)
{
	if ((temp1->azimuth_low == temp2->azimuth_low) &&
			(temp1->azimuth_high == temp2->azimuth_high) &&
			(temp1->pitch_low == temp2->pitch_low) &&
			(temp1->pitch_high == temp2->pitch_high) &&
			(temp1->roll_low == temp2->roll_low) &&
			(temp1->roll_high == temp2->roll_high))
		return 1;
	else
		return 0;
}

static int orientation_exists(struct area_desc *desc,
	struct dev_orientation *orient)
{
	int az_f = 0, p_f = 0, r_f = 0;
	int a = orient->azimuth;
	int p = p_to_360(orient->pitch);
	int r = orient->roll;
	int az_low = desc->azimuth_low;
	int az_high = desc->azimuth_high;
	int p_low = p_to_360(desc->pitch_low);
	int p_high = p_to_360(desc->pitch_high);
	int r_low = desc->roll_low;
	int r_high = desc->roll_high;

	/* check if azimuth is in range */
	if (az_low == az_high)
		az_f = 1;
	if (az_low < az_high)
		az_f = ((az_low <= a) && (a <= az_high)) ? 1 : 0;
	else
		az_f = ((a < az_high) || (a > az_low)) ? 1 : 0;

	/* check if pitch is in range */
	if (p_low <= p_high)
		p_f = ((p_low <= p) && (p <= p_high)) ? 1 : 0;
	else
		p_f = ((p < p_high) || (p > p_low)) ? 1 : 0;

	/* check if roll is in range */
	r_f = ((r_low <= r) && (r <= r_high)) ? 1 : 0;

	if (az_f && p_f && r_f)
		return 1;
	else
		return 0;
}

static struct area_desc *check_if_exists_in_list(struct area_desc *desc)
{
	struct list_head *pos;
	struct area_desc *tmp;
	list_for_each(pos, &desc_head.list) {
		tmp = list_entry(pos, struct area_desc, list);
		if (area_desc_equals(tmp, desc))
			return tmp;
	}
	return NULL;
}

static struct area_desc *create_desc_node
	(struct orientation_range *o_range, int init_flag)
{
	struct area_desc *new_desc;
	new_desc = kmalloc(sizeof(*new_desc), GFP_KERNEL);
	new_desc->num_rl = 0;
	new_desc->num_wl = 0;

	/* initialize ranges */
	new_desc->azimuth_low = adjust_azimuth(o_range->orient.azimuth
		- o_range->azimuth_range);
	new_desc->azimuth_high = adjust_azimuth(o_range->orient.azimuth
		+ o_range->azimuth_range);
	new_desc->pitch_low = adjust_pitch(o_range->orient.pitch
		- o_range->pitch_range);
	new_desc->pitch_high = adjust_pitch(o_range->orient.pitch
		+ o_range->pitch_range);
	new_desc->roll_low = adjust_roll(o_range->orient.roll
		- o_range->roll_range);
	new_desc->roll_high = adjust_roll(o_range->orient.roll
		+ o_range->roll_range);


	if (init_flag) {
		/* initialize rw semaphore inside area_desc */
		init_rwsem(&new_desc->rwsem);
		/* initialize ps_waiting queue */
		INIT_LIST_HEAD(&new_desc->ps_waiting.list);
		new_desc->is_ps_waiting_init = INITIALIZED;

		/* initialize ps_granted list */
		INIT_LIST_HEAD(&new_desc->ps_granted.list);
		new_desc->is_ps_granted_init = INITIALIZED;

		/* sys_ps_waiting */
		init_waitqueue_head(&new_desc->sys_ps_waiting);
		new_desc->is_sys_ps_waiting_init = INITIALIZED;
	}
	return new_desc;
}

static struct area_desc *add_to_area_desc_list(struct area_desc *new_node)
{
	struct area_desc *desc_exist;
	down_write(&mr_rwsem);

	if (desc_head_size == -1) {
		INIT_LIST_HEAD(&desc_head.list);
		desc_head_size = 0;
	}

	desc_exist = check_if_exists_in_list(new_node);
	if (desc_exist == NULL) {
		list_add(&(new_node->list), &desc_head.list);
		desc_head_size++;
		up_write(&mr_rwsem);
		return new_node;
	} else {
		up_write(&mr_rwsem);
		/* TODO destroy new_node  */
		kfree(new_node);
		return desc_exist;
	}

}

static int add_ps_to_desc(struct area_desc *desc, char lock_type)
{
	struct ps_node *ps;
	DEFINE_WAIT(wait);
	down_write(&mr_rwsem);

	if (desc->is_sys_ps_waiting_init != INITIALIZED)
		return RET_FAILURE;

	/* add ps_node to end of ps_waiting list */
	ps = kmalloc(sizeof(*ps), GFP_KERNEL);
	ps->task = current;
	ps->lock_type = lock_type;
	list_add_tail(&(ps->list), &desc->ps_waiting.list);
	up_write(&mr_rwsem);

	/* add current ps to sys_ps_waiting (waitqueue) */
	/* printk(" >**putting the process[%d] to sleep**\n",
		 current->pid);
	*/
	add_wait_queue(&desc->sys_ps_waiting, &wait);
	set_current_state(TASK_INTERRUPTIBLE);
	schedule();

	/* printk(" >**waking the process[%d] from sleep**\n",
		 current->pid);
	*/
	finish_wait(&desc->sys_ps_waiting, &wait);

	return RET_SUCCESS;
}

static int remove_from_ps_granted(struct area_desc *desc, char lock_type)
{
	struct list_head *pos, *q;
	struct ps_node *temp;

	if (desc == NULL)
		return RET_FAILURE;

	list_for_each_safe(pos, q, &desc->ps_granted.list) {
		temp = list_entry(pos, struct ps_node, list);
		if (temp->task == current && temp->lock_type == lock_type) {
			/* unlock the semaphore in area_desc */
			if (lock_type == READ_LOCK_TYPE)
				up_read(&desc->rwsem);
			else if (lock_type == WRITE_LOCK_TYPE)
				up_write(&desc->rwsem);
			/* remove the ps_node from ps_granted list */
			list_del(&temp->list);
			kfree(temp);
			return RET_SUCCESS;
		}
	}
	return RET_FAILURE;
}

static int grant_lock_for_ps_node(struct area_desc *desc,
	struct ps_node *grant_ps, char lock_type)
{
	if (lock_type == READ_LOCK_TYPE)
		desc->num_rl++;
	else if (lock_type == WRITE_LOCK_TYPE)
		desc->num_wl++;
	else
		return RET_FAILURE;

	/* remove ps_node from ps_waiting */
	list_del(&grant_ps->list);

	/* add ps_node to ps_granted */
	list_add(&grant_ps->list, &desc->ps_granted.list);

	/* wake up process */
	wake_up_process(grant_ps->task);

	return RET_SUCCESS;
}

static void perform_lock_ops(struct area_desc *desc)
{
	struct list_head *pos, *q;
	struct ps_node *temp;
	list_for_each_safe(pos, q, &desc->ps_waiting.list) {
		temp = list_entry(pos, struct ps_node, list);
		if (task_is_dead(temp->task)) {
			/*
			printk(" >DELETING task[%d][%s] from wait
				list\n", temp->task->pid, temp->task->comm);
			*/
			list_del(&temp->list);
			kfree(temp);
			continue;
		}
		if (temp->lock_type == READ_LOCK_TYPE) {
			if (down_read_trylock(&desc->rwsem) != 0) {
				/* grant lock for temp */
				grant_lock_for_ps_node(desc, temp,
					READ_LOCK_TYPE);
				continue;
			} else
				break;
		} else if (temp->lock_type == WRITE_LOCK_TYPE) {
			if (down_write_trylock(&desc->rwsem) != 0) {
				/* grant lock for temp */
				grant_lock_for_ps_node(desc, temp,
					WRITE_LOCK_TYPE);
				break;
			} else
				break;
		}
	}
	list_for_each_safe(pos, q, &desc->ps_granted.list) {
		temp = list_entry(pos, struct ps_node, list);
		if (task_is_dead(temp->task)) {
			/* printk(" >DELETING task[%d][%s] from grant
				list\n", temp->task->pid, temp->task->comm);
			*/
			if (temp->lock_type == READ_LOCK_TYPE)
				up_read(&desc->rwsem);
			else if (temp->lock_type == WRITE_LOCK_TYPE)
				up_write(&desc->rwsem);
			list_del(&temp->list);
			kfree(temp);
			continue;
		}
	}
}

void process_locks(struct dev_orientation *orientation)
{
	struct list_head *pos;
	struct area_desc *tmp;
	/* if desc_head is not initialized return */
	if (desc_head_size <= 0)
		return;
	down_write(&mr_rwsem);
	list_for_each(pos, &desc_head.list) {
		tmp = list_entry(pos, struct area_desc, list);
		if (orientation_exists(tmp, orientation))
			perform_lock_ops(tmp);
	}
	up_write(&mr_rwsem);
}

/* syscall 377 */
SYSCALL_DEFINE1(orientlock_read, struct orientation_range __user *, orient)
{
	struct area_desc *new_node, *desc_for_orient;
	new_node = create_desc_node(orient, 1);
	desc_for_orient = add_to_area_desc_list(new_node);
	add_ps_to_desc(desc_for_orient, READ_LOCK_TYPE);
	return RET_SUCCESS;
}

/* syscall 378 */
SYSCALL_DEFINE1(orientlock_write, struct orientation_range __user *, orient)
{
	struct area_desc *new_node, *desc_for_orient;
	new_node = create_desc_node(orient, 1);
	desc_for_orient = add_to_area_desc_list(new_node);
	add_ps_to_desc(desc_for_orient, WRITE_LOCK_TYPE);
	return RET_SUCCESS;
}

/* syscall 379 */
SYSCALL_DEFINE1(orientunlock_read, struct orientation_range __user *, orient)
{
	struct area_desc *new_node, *desc_exists;
	new_node = create_desc_node(orient, 0);
	down_write(&mr_rwsem);
	desc_exists = check_if_exists_in_list(new_node);
	/*destroy new_node */
	kfree(new_node);
	if (desc_exists == NULL) {
		up_write(&mr_rwsem);
		return RET_FAILURE;
	}
	if (remove_from_ps_granted(desc_exists,
		READ_LOCK_TYPE) == RET_FAILURE) {
		up_write(&mr_rwsem);
		return RET_FAILURE;
	}
	up_write(&mr_rwsem);
	return RET_SUCCESS;
}

/* syscall 380 */
SYSCALL_DEFINE1(orientunlock_write, struct orientation_range __user *, orient)
{
	struct area_desc *new_node, *desc_exists;
	new_node = create_desc_node(orient, 0);
	down_write(&mr_rwsem);
	desc_exists = check_if_exists_in_list(new_node);
	/*destroy new_node */
	kfree(new_node);
	if (desc_exists == NULL) {
		up_write(&mr_rwsem);
		return RET_FAILURE;
	}
	if (remove_from_ps_granted(desc_exists,
		WRITE_LOCK_TYPE) == RET_FAILURE) {
		up_write(&mr_rwsem);
		return RET_FAILURE;
	}
	up_write(&mr_rwsem);
	return RET_SUCCESS;
}
