#include <linux/uaccess.h>
#include <linux/slab.h>
#include <linux/orientation.h>
#include <linux/syscalls.h>
#include <linux/kernel.h>

static struct dev_orientation *curr_orient;
static int is_dev_orientation_init = 0;

/*
static void print_orientation(struct dev_orientation *orient)
{
	printk(" dev_orient[a=%d, p=%d, r=%d]\n",
		orient->azimuth, orient->pitch, orient->roll);
}
*/

SYSCALL_DEFINE1(set_orientation, struct dev_orientation __user *, orient)
{
	if (is_dev_orientation_init == 0 || curr_orient == NULL) {
		is_dev_orientation_init = 1;
		curr_orient = kmalloc(sizeof(struct dev_orientation),
				GFP_KERNEL);
		if (curr_orient == NULL)
			return -ENOMEM;
	}

	if (copy_from_user(curr_orient, orient, sizeof(struct dev_orientation)))
		return -EFAULT;

	process_locks(curr_orient);
	return RET_SUCCESS;
}
