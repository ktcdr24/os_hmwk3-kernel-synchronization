#ifndef _LINUX_ORIENTATION_H
#define _LINUX_ORIENTATION_H

#define RET_SUCCESS 0
#define RET_FAILURE -1

struct dev_orientation {
	int azimuth; /* angle between the magnetic north
			and the Y axis, around the Z axis
			(0<=azimuth<360)
			0=North, 90=East, 180=South, 270=West */
	int pitch;   /* rotation around the X-axis: -180<=pitch<=180 */
	int roll;    /* rotation around Y-axis: +Y == -roll,
			-90<=roll<=90 */
};

struct orientation_range {
	struct dev_orientation orient;  /*  device orientation */
	unsigned int azimuth_range;     /* +/- degrees around Z-axis */
	unsigned int pitch_range;       /* +/- degrees around X-axis */
	unsigned int roll_range;        /* +/- degrees around Y-axis */
};

void process_locks(struct dev_orientation *orientation);

#endif /* _LINUX_ORIENTATION_H */
