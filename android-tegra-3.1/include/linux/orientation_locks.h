#ifndef _LINUX_ORIENTATION_LOCKS_H
#define _LINUX_ORIENTATION_LOCKS_H
#include <linux/semaphore.h>


#include <linux/wait.h>
#include <linux/sched.h>

#define NOT_INITIALIZED 0
#define INITIALIZED 1

struct ps_node {
	struct list_head list;
	struct task_struct *task;
	char lock_type;
};

struct area_desc {
	struct list_head list;
	int num_rl;
	int num_wl;

	int azimuth_low;
	int azimuth_high;

	int pitch_low;
	int pitch_high;
	int roll_low;
	int roll_high;
	/* Queue of the waiting processes */
	struct ps_node ps_waiting;
	int is_ps_waiting_init;

	/* List of ps granted locks */
	struct ps_node ps_granted;
	int is_ps_granted_init;
	struct rw_semaphore rwsem;

	wait_queue_head_t sys_ps_waiting;
	int is_sys_ps_waiting_init;
};

#endif /* _LINUX_ORIENTATION_LOCKS_H */
