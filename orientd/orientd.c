/*
 * Columbia University
 * COMS W4118 Fall 2012
 * Homework 3 - orientd
 *
 */
#include <bionic/errno.h> /* Google does things a little different...*/
#include <fcntl.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include "../android-tegra-3.1/include/linux/akm8975.h"

/* added for set_orientation syscall */
#include <sys/syscall.h>
#include "../android-tegra-3.1/include/linux/orientation.h"

#include <string.h>
#include <errno.h>

#include <hardware/hardware.h>
#include <hardware/sensors.h> /* <-- This is a good place to look! */

/* from sensors.c */
#define ID_ACCELERATION   (0)
#define ID_MAGNETIC_FIELD (1)
#define ID_ORIENTATION	  (2)
#define ID_TEMPERATURE	  (3)

#define SENSORS_ACCELERATION   (1<<ID_ACCELERATION)
#define SENSORS_MAGNETIC_FIELD (1<<ID_MAGNETIC_FIELD)
#define SENSORS_ORIENTATION    (1<<ID_ORIENTATION)
#define SENSORS_TEMPERATURE    (1<<ID_TEMPERATURE)

/* the period at which the orientation sensor will update */
#define ORIENTATION_UPDATE_PERIOD_MS 500

#define FLOAT_COMPARISION_EPSILON (0.001)

/* set to 1 for a bit of debug output */
#if 0
	#define dbg_compass(fmt, ...) printf("compass: " fmt, ## __VA_ARGS__)
#else
	#define dbg_compass(fmt, ...)
#endif

static int effective_sensor;

/* helper functions which you should use */
static int open_compass(struct sensors_module_t **hw_module,
			struct sensors_poll_device_t **poll_device);
static void enumerate_sensors(const struct sensors_module_t *sensors);

static int poll_sensor_data(struct sensors_poll_device_t *sensors_device,
		float orient[3])
{
	int flag_orient_unchanged = 1;
	const size_t numEventMax = 16;
	const size_t minBufferSize = numEventMax;
	sensors_event_t buffer[minBufferSize];
	ssize_t count = sensors_device->poll(sensors_device, buffer,
			minBufferSize);
	int i;
	for (i = 0; i < count; ++i) {
		/* Find compass sensor */
		if (buffer[i].sensor != effective_sensor)
			continue;

		/* At this point we should have valid data */
		dbg_compass("Orientation: azimuth=%0.2f, pitch=%0.2f, "\
			"roll=%0.2f\n", buffer[i].orientation.azimuth,
			buffer[i].orientation.pitch,
			buffer[i].orientation.roll);

		orient[0] = buffer[i].orientation.azimuth;
		orient[1] = buffer[i].orientation.pitch;
		orient[2] = buffer[i].orientation.roll;
		flag_orient_unchanged = 0;
	}
	if (flag_orient_unchanged == 0)
		return 0; /* return 0 if t_orient changed */
	return 1; /* return 0 if t_orient NOT changed */
}

static int is_equal(float a[3], float b[3])
{
	int i;

	for (i = 0; i < 3; ++i) {
		if (fabs(a[i] - b[i]) < FLOAT_COMPARISION_EPSILON)
			continue;
		else
			return 0;
	}
	return 1;
}

/* entry point of orientd: fill in daemon implementation
   where indicated */
int main(int argc, char **argv)
{
	struct dev_orientation *t_orient;
	float curr_orient[3];
	float prev_orient[3];
	struct sensors_module_t *sensors_module = NULL;
	struct sensors_poll_device_t *sensors_device = NULL;

	t_orient = (struct dev_orientation *)malloc(sizeof(
				struct dev_orientation));
	effective_sensor = -1;
	int f_return = fork();
	if (f_return < 0) {
		printf("error: %s", strerror(errno));
		exit(EXIT_FAILURE);
	} else if (f_return == 0) {
		/* child processs */
		/*
		refered: http://www.thegeekstuff.com/2012/02/c-daemon-process/
		 */
		/* run the process in new session */
		pid_t sid = setsid();
		if (sid < 0) {
			printf("error: %s\n", strerror(errno));
			exit(EXIT_FAILURE);
		}

		/* change directory to root */
		chdir("/");

		/* close the open File Descriptors */
		close(STDIN_FILENO);
		close(STDOUT_FILENO);
		close(STDERR_FILENO);

		/* use the log file /data/misc/daemon_log.txt for debug */
		FILE *fp = NULL;
		fp = fopen("/data/misc/daemon_log.txt", "w+");
		if (open_compass(&sensors_module,
				 &sensors_device) < 0) {
			printf("error: open_compass failed\n");
			exit(EXIT_FAILURE);
		}

		enumerate_sensors(sensors_module);

		int i;
		while (1) {
			/* back up previous t_orient */
			for (i = 0; i < 3; ++i)
				prev_orient[i] = curr_orient[i];

			/* continue if the poll_sensor_data fails */
			if(poll_sensor_data(sensors_device, curr_orient) != 0)
				continue;

			/* continue if the values didnt change at all */
			if (is_equal(curr_orient, prev_orient))
				continue;

			t_orient->azimuth = curr_orient[0];
			t_orient->pitch = curr_orient[1];
			t_orient->roll = curr_orient[2];
			syscall(376, t_orient);
		}
	} else {
		/* parent process */
		exit(EXIT_SUCCESS);
	}

	/* this line will never be reached */
	return 0;
}

/*                DO NOT MODIFY BELOW THIS LINE                    */
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

static int open_compass(struct sensors_module_t **mSensorModule,
			struct sensors_poll_device_t **mSensorDevice)
{
   
	int err = hw_get_module(SENSORS_HARDWARE_MODULE_ID,
				     (hw_module_t const**)mSensorModule);

	if (err) {
		printf("couldn't load %s module (%s)",
			SENSORS_HARDWARE_MODULE_ID, strerror(-err));
	}

	if (!*mSensorModule)
		return -1;

	err = sensors_open(&((*mSensorModule)->common), mSensorDevice);

	if (err) {
		printf("couldn't open device for module %s (%s)",
			SENSORS_HARDWARE_MODULE_ID, strerror(-err));
	}

	if (!*mSensorDevice)
		return -1;

	const struct sensor_t *list;
	ssize_t count = (*mSensorModule)->get_sensors_list(*mSensorModule, &list);
	size_t i;
	for (i=0 ; i<(size_t)count ; i++)
		(*mSensorDevice)->activate(*mSensorDevice, list[i].handle, 1);

	return 0;
}

static void enumerate_sensors(const struct sensors_module_t *sensors)
{
	int nr, s;
	const struct sensor_t *slist = NULL;
	if (!sensors)
		printf("going to fail\n");

	nr = sensors->get_sensors_list((struct sensors_module_t *)sensors,
					&slist);
	if (nr < 1 || slist == NULL) {
		printf("no sensors!\n");
		return;
	}

	for (s = 0; s < nr; s++) {
		printf("%s (%s) v%d\n\tHandle:%d, type:%d, max:%0.2f, "
			"resolution:%0.2f \n", slist[s].name, slist[s].vendor,
			slist[s].version, slist[s].handle, slist[s].type,
			slist[s].maxRange, slist[s].resolution);

		/* Awful hack to make it work on emulator */
		if (slist[s].type == 2 && slist[s].handle == 1)
			effective_sensor = 2;
		else if (slist[s].type == 3 && slist[s].handle == 3)
			effective_sensor = SENSOR_TYPE_ORIENTATION;

                switch (slist[s].type) {
                    case SENSOR_TYPE_ORIENTATION:
			printf("SENSOR_TYPE_ORIENTATION\n");
                        break;
                    case SENSOR_TYPE_GYROSCOPE:
			printf("SENSOR_TYPE_GYROSCOPE\n");
                        break;
                    case SENSOR_TYPE_GRAVITY:
			printf("SENSOR_TYPE_GRAVITY\n");
			break;
                    case SENSOR_TYPE_LINEAR_ACCELERATION:
			printf("SENSOR_TYPE_ACCELERATION\n");
			break;
                    case SENSOR_TYPE_ROTATION_VECTOR:
			printf("SENSOR_TYPE_ROTATION_VECTOR\n");
			break;
                }
	}
}
